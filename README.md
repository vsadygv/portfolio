# V's Portfolio

## About Me

Hello, I'm V, a passionate embedded systems engineer with a strong interest in product development. I have over 10 years of experience in software development, primarily focusing on embedded systems. I'm self-taught on various skills including electronics,3D printing,model design and more!


## Skills

- Programming Languages: C/C++, Python, C#, Java, MicroPython, Assembly, GLSL, Bash Scripting
- Tools: Linux/Unix, AutoCAD, LibreCAD, EagleCAD, KiCAD, MULTISIM, QMK, VIM, Arduino, git, Jenkins, DIPtrace, Solidworks
- Soldering(TMH and SMD)
- 3D Modeling
- 3D Printing
- Simulating (Electrical and Mechanical)
- Unit Testing
- Embedded Systems Development
- Finite Element Analysis
- Truss Engineering
- Solidworks.ISOPE
## Certifications and Achievements

- Level 1 Rocketry Certification
- Technician Ham Radio License
- <a href="https://cyberskyline.com/report/CYRR6EML6CUU" target=_blank>National Cyber League Capture The Flag Solo Score</a> 
- <a href="https://cyberskyline.com/report/R54JKD3TF82D" target=_blank>National Cyber League Capture The Flag Team Score</a>
- <a href="https://2024.irisc.tf/user-id-115.html" target=_blank>IrisCTF Score</a>
## Goals

- Level 2 Rocketry Certification
- General Ham Radio License
- Score in the top 10 in a CTF
- Expand knowlege of RISC-V architecture
- Develop custom-made devices for personal projects

## Contact Me

- Email: <a href=mailto:v@vsadygv.com target=_blank >v@vsadygv.com</a>
